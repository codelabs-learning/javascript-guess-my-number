'use strict';

import { updateTextContent, generateRandomNumber } from './functions.js';

let highScore = 0;
let message = '';
let range = [1, 20];
let score = range[1];

updateTextContent('.between', `(Between ${range[0]} and ${range[1]})`);
updateTextContent('.highscore', highScore);
updateTextContent('.score', score);

/**
 * Generate random number from 1-20.
 */
let secretNumber = generateRandomNumber(range[0], range[1]);
console.log(secretNumber);

/**
 * Bind Event Listener to Element
 */

document.querySelector('.check').addEventListener('click', function () {
  const guess = +document.querySelector('.guess').value;
  // when we request a user input the result is always text.
  // we convert it to number either by using Number(document...) or +document...
  //console.log(guess, typeof guess);

  // Check for empty value (if the user clicked the button without typing a number first).
  // the result of Number("") would be 0 and 0 is a falsy value.

  if (!guess) {
    updateTextContent('.message', 'No number...');
    // When use finds the number.
  } else if (guess === secretNumber) {
    message = 'Correct number!!!';
    //This will apply an inline style.
    document.querySelector('body').style.backgroundColor = '#60b347'; //always string when changing the style attribute.
    updateTextContent('.number', guess);
    if (score > highScore) {
      highScore = score;
      updateTextContent('.highscore', highScore);
    }
  } else {
    //You have guess wrong...
    message = 'You lost!!!';
    if (guess > secretNumber && score > 1) message = 'Too high!!!';
    if (guess < secretNumber && score > 1) message = 'Too low!!!';

    score--;
    updateTextContent('.score', score);
  }

  updateTextContent('.message', message);
});

document.querySelector('.again').addEventListener('click', function () {
  score = range[1];
  updateTextContent('.score', score);
  updateTextContent('.message', 'Start guessing...');
  document.querySelector('body').style.backgroundColor = '#222';
  updateTextContent('.number', '?');
  document.querySelector('.guess').value = '';
  secretNumber = generateRandomNumber(range[0], range[1]);
  console.log(secretNumber);
});
