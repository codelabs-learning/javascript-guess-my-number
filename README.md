A javascript game to guess a random number.
-----


This is a udemy course project that implements the following:

-- DOM Manipulation
-- Basic eventListeners
-- ES6 Modules
-- Unit Testing with Jest
-- Jest Code Coverage

Installation
-----


Run `npm install`

Test `npm test` or `npm test --corerage` to generate coverage report.
