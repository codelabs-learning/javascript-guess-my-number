export function updateTextContent(selector, value) {
  document.querySelector(selector).textContent = value;
}

export function generateRandomNumber(start, end) {
  return Math.trunc(Math.random() * end) + start;
}
