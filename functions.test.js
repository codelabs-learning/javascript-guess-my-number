import { expect } from '@jest/globals';
import { updateTextContent, generateRandomNumber } from './functions.js';

test('if generates random number between 1 and 10', () => {
  const number = generateRandomNumber(1, 10);

  expect(number).toBeGreaterThanOrEqual(1);
  expect(number).toBeLessThanOrEqual(20);
});

test('if updates element content', () => {
  document.body.innerHTML = `
    <section class="right">
        <p class="message">Start guessing...</p>
        <p class="label-score">💯 Score: <span class="score">20</span></p>
    </section>
  `;
  updateTextContent('.message', 'my message');
  expect(document.querySelector('.message').textContent).toBe('my message');
});

test('if applies random number as initial score', () => {
  document.body.innerHTML = `
      <section class="right">
          <p class="message">Start guessing...</p>
          <p class="label-score">💯 Score: <span class="score">20</span></p>
      </section>
    `;
  const number = generateRandomNumber(1, 10);

  updateTextContent('.score', number);
  expect(+document.querySelector('.score').textContent).toBe(number);
});
